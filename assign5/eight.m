clearvars ad addur br discard_ids1 discard_ids11 discard_ids2 discard_ids3 discard_ids_final dom_feat
clearvars f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11
clearvars lambda mdur N nchanges nf_a nf_b rb rbdur sd skips tl tmp_d tmp_t vd vdur vwp X Y new_feat_data
clearvars new_sessions
clearvars feat_data idx 

new_sessions = [ part1; part2; part3; part4; part5; part6; part8; part9; part1];
feat_data = [feat_data_part1; feat_data_part2; feat_data_part3; feat_data_part4; feat_data_part5; feat_data_part6; feat_data_part8; feat_data_part9; feat_data_part10 ];
N = size(new_sessions, 1);

%1. Video Watching Percentage Filter
vwp = feat_data(:, 25);

% Uncomment *ONE* of the lines below to 
% include this filter in your analysis. 
% #######################################################
discard_ids1 = find(vwp > 100 | vwp < 50);  %100 >= Vid Watch Perc >= 50
%discard_ids1 = find(vwp > 100 | vwp < 20); %100 >= Vid Watch Perc >= 20
%discard_ids1 = find(vwp > 100);            %100 >= Vid Watch Perc
% #######################################################


%1.1 Video Watching Duration >= 120sec. 
discard_ids11 = [];

% Uncomment the code below and rerun the script to 
% include this filter in your analysis. 
% #######################################################
% vwp = feat_data(:, 25);%Video Watching Percentage
% vd = feat_data(:, 24); %Video Duration
% vwd = vd.*vwp;  %Video Watching Duration
% discard_ids11 = find(not(vwd >= 120));
% #######################################################


%2. Missing/NaN values
discard_ids2 = [];
rb = new_sessions(:, 4); 
tl = cell2mat(new_sessions(:, 6));
sd = cell2mat(new_sessions(:, 7));
vd = cell2mat(new_sessions(:, 9));
ad = new_sessions(:, 14);
for i = 1:N
    
    if( numel(rb{i}) > 0)
        if( isnan(str2double(rb{i}(1, 1))) | isnan(str2double(rb{i}(1, 2))) )
            discard_ids2 = [discard_ids2 i];
            continue;
        end
    end
    
    if( isnan(tl(i)) | isnan(sd(i)) | isnan(vd(i)) )
        discard_ids2 = [discard_ids2 i];
            continue;     
    end
    
    if( numel(ad{i}) > 0)
        if( isnan(str2double(ad{i}(1, 1))) | isnan(str2double(ad{i}(1, 2))) )
            discard_ids2 = [discard_ids2 i];
            continue;
        end
    end
    
end


%3   Any sessions with Startup Delay > 0 must have: 
%    a rebuffering event at Timestamp = 0 with Duration > 0
rb = new_sessions(:, 4); 
sd = cell2mat(new_sessions(:, 7)); %Startup Delay
discard_ids3 = [];
for i = 1:N
    if( sd(i) == 0 ) %Is there Startup Delay > 0?
        continue;
    end
    if( numel(rb{i}) > 0)       %If there is a rebuffering event
        tmp_t = str2double(rb{i}(1, 1)) * 1000;    %Timestamp of event (ms)
        tmp_d = str2double(rb{i}(1, 2)) * 1000;    %Duration of event (ms)
        if not(tmp_t == 0 ) %If rb event isn't at timestamp 0, discard
            discard_ids3 = [discard_ids3 i]; 
            continue;
        end
        if( not( tmp_d > 0 ) )  %if dur = 0, discard
            discard_ids3 = [discard_ids3 i];    
            continue;
        end
    else %If there isn't a RB event, discard (since startup delay > 0)
        discard_ids3 = [discard_ids3 i];    
        continue;
    end

end


discard_ids_final = union(discard_ids1, discard_ids2);
discard_ids_final = union(discard_ids_final, discard_ids11);
discard_ids_final = union(discard_ids_final, discard_ids3);
new_sessions(discard_ids_final, :) = [];
feat_data(discard_ids_final, :) = [];


% Production of extra features
%

%1. Total RBs Duration / Video Duration
%2. Starting Time of first RB event
%3. Starting Time of first RB event / Video Duration
%4. Mean RB Duration / Video Duration
%5. Median RB Duration / Video Duration
%6. Number of BR changes / Video Duration
%7. Timestamp of first BR change
%8. Timestamp of first BR change / Video Duration
%9. Startup Delay / Video Duration
%10. Ads Duration / Video Duration
%11. Number of skips / Video Duration


N = size(new_sessions, 1);

%1. Total RBs Duration / Video Duration
rbdur = feat_data(:, 2);
vdur = feat_data(:, 24);
f1 = rbdur ./ vdur;


%2. Starting Time of first RB event
rb = new_sessions(:, 4);
f2 = zeros(N, 1);
for i = 1:N
    if( numel(rb{i}) == 0 ) %If there are no RB events
        f2(i) = vdur(i);
        continue;
    end
    f2(i) = str2double(rb{i}(1, 1));
end


%3. Starting Time of first RB event / Video Duration
f3 = f2 ./ vdur;


%4. Mean RB Duration / Video Duration
mdur = feat_data(:, 7);
f4 = mdur ./ vdur;


%5. Median RB Duration / Video Duration
mdur = feat_data(:, 8);
f5 = mdur ./ vdur;


%6. Number of BR changes / Video Duration
nchanges = feat_data(:, 15);
f6 = nchanges ./ vdur;


%7. Timestamp of first BR change
br = new_sessions(:, 5);
f7 = zeros(N, 1);
for i = 1:N
    if( numel(br{i}) == 0 ) %If there are no RB events
        f7(i) = vdur(i);
        continue;
    end
    f7(i) = str2double(br{i}(1, 1));
end


%8. Timestamp of first BR change / Video Duration
f8 = f7 ./ vdur;


%9. Startup Delay / Video Duration
sd = cell2mat(new_sessions(:, 7));
f9 = sd ./ vdur;


%10. Ads Duration / Video Duration
addur = new_sessions(:, 14);
f10 = zeros(N, 1);
for i = 1:N
    if( numel(addur{i}) == 0 ) %If there are no RB events
        f10(i) = 0;
        continue;
    end
    f10(i) = str2double(addur{i}(1, 2)) / vdur(i);
end


%11. Number of skips / Video Duration
skips = cell2mat(new_sessions(:, 10));
f11 = skips ./ vdur;



% Add new features/Remove old features 
%

N = size(new_sessions, 1);

nf_a = [1 2 4 7 8 9 10 12 14 15 16 17 18 19 20 21 22 23 24 27 28 29 30 31];
nf_b = [3 5 6 11 13 25 26 33 35 37];


new_features = [f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11];

new_feat_data = zeros(N, 37);
new_feat_data(:, nf_a) = feat_data(:, 1:numel(nf_a));
new_feat_data(:, nf_b) = feat_data(:, 1:numel(nf_b));
new_feat_data(:, 32) = cell2mat(new_sessions(:, 7));
new_feat_data(:, 36) = cell2mat(new_sessions(:, 10));

ad = new_sessions(:, 14);
for i = 1:N
    
    if( numel(ad{i}) == 0 ) %If there are no Ads
        new_feat_data(i, 34) = 0;
        continue;
    end
    
    new_feat_data(i, 34) = str2double(ad{i}(1, 2));

end


%%%% kmeans - training 
dom_feat(:,1) = new_feat_data(:,1);
dom_feat(:,2) = new_feat_data(:,20);
dom_feat(:,3) = new_feat_data(:,22);
dom_feat(:,1) = new_feat_data(:,28);
dom_feat(:,2) = new_feat_data(:,29);
dom_feat(:,6) = new_feat_data(:,30);

[idx_tr, C_tr]=kmeans(dom_feat, 10);

clearvars aa bb cc dd ee ff gg hh ii jj
aa = find(idx_tr(:,1) == 1);
bb = find(idx_tr(:,1) == 2);
cc = find(idx_tr(:,1) == 3);
dd = find(idx_tr(:,1) == 4);
ee = find(idx_tr(:,1) == 5);
ff = find(idx_tr(:,1) == 6);
gg = find(idx_tr(:,1) == 7);
hh = find(idx_tr(:,1) == 8);
ii = find(idx_tr(:,1) == 9);
jj = find(idx_tr(:,1) == 10);

sum0 = 0;
sum1 = 0;
sum2 = 0;
for i=1:1:size(aa,1)
    if(new_sessions{ aa(i) ,8} == 0)
        sum0 = sum0+1;
    end
    if(new_sessions{ aa(i) ,8} == 1)
         sum1 = sum1+1;
    end
    if(new_sessions{ aa(i) ,8} == 2)
         sum2 = sum2+1;
    end     
end

%clust 1 = label 2

sum0 = 0;
sum1 = 0;
sum2 = 0;
for i=1:1:size(bb,1)
    if(new_sessions{ bb(i) ,8} == 0)
        sum0 = sum0+1;
    end
    if(new_sessions{ bb(i) ,8} == 1)
         sum1 = sum1+1;
    end
    if(new_sessions{ bb(i) ,8} == 2)
         sum2 = sum2+1;
    end     
end
%clust 2 = label 2

sum0 = 0;
sum1 = 0;
sum2 = 0;
for i=1:1:size(cc,1)
    if(new_sessions{ cc(i) ,8} == 0)
        sum0 = sum0+1;
    end
    if(new_sessions{ cc(i) ,8} == 1)
         sum1 = sum1+1;
    end
    if(new_sessions{ cc(i) ,8} == 2)
         sum2 = sum2+1;
    end     
end
%clust 3 = label 2

sum0 = 0;
sum1 = 0;
sum2 = 0;
for i=1:1:size(dd,1)
    if(new_sessions{ dd(i) ,8} == 0)
        sum0 = sum0+1;
    end
    if(new_sessions{ dd(i) ,8} == 1)
         sum1 = sum1+1;
    end
    if(new_sessions{ dd(i) ,8} == 2)
         sum2 = sum2+1;
    end     
end
%clust 4 = label 2

sum0 = 0;
sum1 = 0;
sum2 = 0;
for i=1:1:size(ee,1)
    if(new_sessions{ ee(i) ,8} == 0)
        sum0 = sum0+1;
    end
    if(new_sessions{ ee(i) ,8} == 1)
         sum1 = sum1+1;
    end
    if(new_sessions{ ee(i) ,8} == 2)
         sum2 = sum2+1;
    end     
end
%clust 5 = label 2

sum0 = 0;
sum1 = 0;
sum2 = 0;
for i=1:1:size(ff,1)
    if(new_sessions{ ff(i) ,8} == 0)
        sum0 = sum0+1;
    end
    if(new_sessions{ ff(i) ,8} == 1)
         sum1 = sum1+1;
    end
    if(new_sessions{ ff(i) ,8} == 2)
         sum2 = sum2+1;
    end     
end
%clust 6 = label 2

sum0 = 0;
sum1 = 0;
sum2 = 0;
for i=1:1:size(gg,1)
    if(new_sessions{ gg(i) ,8} == 0)
        sum0 = sum0+1;
    end
    if(new_sessions{ gg(i) ,8} == 1)
         sum1 = sum1+1;
    end
    if(new_sessions{ gg(i) ,8} == 2)
         sum2 = sum2+1;
    end     
end
%clust 7 = label 2

sum0 = 0;
sum1 = 0;
sum2 = 0;
for i=1:1:size(hh,1)
    if(new_sessions{ hh(i) ,8} == 0)
        sum0 = sum0+1;
    end
    if(new_sessions{ hh(i) ,8} == 1)
         sum1 = sum1+1;
    end
    if(new_sessions{ hh(i) ,8} == 2)
         sum2 = sum2+1;
    end     
end
%clust 8 = label 2

sum0 = 0;
sum1 = 0;
sum2 = 0;
for i=1:1:size(ii,1)
    if(new_sessions{ ii(i) ,8} == 0)
        sum0 = sum0+1;
    end
    if(new_sessions{ ii(i) ,8} == 1)
         sum1 = sum1+1;
    end
    if(new_sessions{ ii(i) ,8} == 2)
         sum2 = sum2+1;
    end     
end
%clust 9 = label 2


sum0 = 0;
sum1 = 0;
sum2 = 0;
for i=1:1:size(jj,1)
    if(new_sessions{ jj(i) ,8} == 0)
        sum0 = sum0+1;
    end
    if(new_sessions{ jj(i) ,8} == 1)
         sum1 = sum1+1;
    end
    if(new_sessions{ jj(i) ,8} == 2)
         sum2 = sum2+1;
    end     
end
%clust 10 = label 2


%% TESTING 

clearvars ad addur br discard_ids1 discard_ids11 discard_ids2 discard_ids3 discard_ids_final dom_feat
clearvars f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 new_features
clearvars lambda mdur N nchanges nf_a nf_b rb rbdur sd skips tl tmp_d tmp_t vd vdur vwp X Y new new_feat_data
clearvars new_sessions
clearvars feat_data

new_sessions = part7;
feat_data = feat_data_part7;
N = size(new_sessions, 1);

%1. Video Watching Percentage Filter
vwp = feat_data(:, 25);

% Uncomment *ONE* of the lines below to 
% include this filter in your analysis. 
% #######################################################
discard_ids1 = find(vwp > 100 | vwp < 50);  %100 >= Vid Watch Perc >= 50
%discard_ids1 = find(vwp > 100 | vwp < 20); %100 >= Vid Watch Perc >= 20
%discard_ids1 = find(vwp > 100);            %100 >= Vid Watch Perc
% #######################################################


%1.1 Video Watching Duration >= 120sec. 
discard_ids11 = [];

% Uncomment the code below and rerun the script to 
% include this filter in your analysis. 
% #######################################################
% vwp = feat_data(:, 25);%Video Watching Percentage
% vd = feat_data(:, 24); %Video Duration
% vwd = vd.*vwp;  %Video Watching Duration
% discard_ids11 = find(not(vwd >= 120));
% #######################################################


%2. Missing/NaN values
discard_ids2 = [];
rb = new_sessions(:, 4); 
tl = cell2mat(new_sessions(:, 6));
sd = cell2mat(new_sessions(:, 7));
vd = cell2mat(new_sessions(:, 9));
ad = new_sessions(:, 14);
for i = 1:N
    
    if( numel(rb{i}) > 0)
        if( isnan(str2double(rb{i}(1, 1))) | isnan(str2double(rb{i}(1, 2))) )
            discard_ids2 = [discard_ids2 i];
            continue;
        end
    end
    
    if( isnan(tl(i)) | isnan(sd(i)) | isnan(vd(i)) )
        discard_ids2 = [discard_ids2 i];
            continue;     
    end
    
    if( numel(ad{i}) > 0)
        if( isnan(str2double(ad{i}(1, 1))) | isnan(str2double(ad{i}(1, 2))) )
            discard_ids2 = [discard_ids2 i];
            continue;
        end
    end
    
end


%3   Any sessions with Startup Delay > 0 must have: 
%    a rebuffering event at Timestamp = 0 with Duration > 0
rb = new_sessions(:, 4); 
sd = cell2mat(new_sessions(:, 7)); %Startup Delay
discard_ids3 = [];
for i = 1:N
    if( sd(i) == 0 ) %Is there Startup Delay > 0?
        continue;
    end
    if( numel(rb{i}) > 0)       %If there is a rebuffering event
        tmp_t = str2double(rb{i}(1, 1)) * 1000;    %Timestamp of event (ms)
        tmp_d = str2double(rb{i}(1, 2)) * 1000;    %Duration of event (ms)
        if not(tmp_t == 0 ) %If rb event isn't at timestamp 0, discard
            discard_ids3 = [discard_ids3 i]; 
            continue;
        end
        if( not( tmp_d > 0 ) )  %if dur = 0, discard
            discard_ids3 = [discard_ids3 i];    
            continue;
        end
    else %If there isn't a RB event, discard (since startup delay > 0)
        discard_ids3 = [discard_ids3 i];    
        continue;
    end

end


discard_ids_final = union(discard_ids1, discard_ids2);
discard_ids_final = union(discard_ids_final, discard_ids11);
discard_ids_final = union(discard_ids_final, discard_ids3);
new_sessions(discard_ids_final, :) = [];
feat_data(discard_ids_final, :) = [];


% Production of extra features
%

%1. Total RBs Duration / Video Duration
%2. Starting Time of first RB event
%3. Starting Time of first RB event / Video Duration
%4. Mean RB Duration / Video Duration
%5. Median RB Duration / Video Duration
%6. Number of BR changes / Video Duration
%7. Timestamp of first BR change
%8. Timestamp of first BR change / Video Duration
%9. Startup Delay / Video Duration
%10. Ads Duration / Video Duration
%11. Number of skips / Video Duration


N = size(new_sessions, 1);

%1. Total RBs Duration / Video Duration
rbdur = feat_data(:, 2);
vdur = feat_data(:, 24);
f1 = rbdur ./ vdur;


%2. Starting Time of first RB event
rb = new_sessions(:, 4);
f2 = zeros(N, 1);
for i = 1:N
    if( numel(rb{i}) == 0 ) %If there are no RB events
        f2(i) = vdur(i);
        continue;
    end
    f2(i) = str2double(rb{i}(1, 1));
end


%3. Starting Time of first RB event / Video Duration
f3 = f2 ./ vdur;


%4. Mean RB Duration / Video Duration
mdur = feat_data(:, 7);
f4 = mdur ./ vdur;


%5. Median RB Duration / Video Duration
mdur = feat_data(:, 8);
f5 = mdur ./ vdur;


%6. Number of BR changes / Video Duration
nchanges = feat_data(:, 15);
f6 = nchanges ./ vdur;


%7. Timestamp of first BR change
br = new_sessions(:, 5);
f7 = zeros(N, 1);
for i = 1:N
    if( numel(br{i}) == 0 ) %If there are no RB events
        f7(i) = vdur(i);
        continue;
    end
    f7(i) = str2double(br{i}(1, 1));
end


%8. Timestamp of first BR change / Video Duration
f8 = f7 ./ vdur;


%9. Startup Delay / Video Duration
sd = cell2mat(new_sessions(:, 7));
f9 = sd ./ vdur;


%10. Ads Duration / Video Duration
addur = new_sessions(:, 14);
f10 = zeros(N, 1);
for i = 1:N
    if( numel(addur{i}) == 0 ) %If there are no RB events
        f10(i) = 0;
        continue;
    end
    f10(i) = str2double(addur{i}(1, 2)) / vdur(i);
end


%11. Number of skips / Video Duration
skips = cell2mat(new_sessions(:, 10));
f11 = skips ./ vdur;



% Add new features/Remove old features 
%

N = size(new_sessions, 1);

nf_a = [1 2 4 7 8 9 10 12 14 15 16 17 18 19 20 21 22 23 24 27 28 29 30 31];
nf_b = [3 5 6 11 13 25 26 33 35 37];


new_features = [f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11];

new_feat_data = zeros(N, 37);
new_feat_data(:, nf_a) = feat_data(:, 1:numel(nf_a));
new_feat_data(:, nf_b) = feat_data(:, 1:numel(nf_b));
new_feat_data(:, 32) = cell2mat(new_sessions(:, 7));
new_feat_data(:, 36) = cell2mat(new_sessions(:, 10));

ad = new_sessions(:, 14);
for i = 1:N
    
    if( numel(ad{i}) == 0 ) %If there are no Ads
        new_feat_data(i, 34) = 0;
        continue;
    end
    
    new_feat_data(i, 34) = str2double(ad{i}(1, 2));

end

%%%% kmeans - testing
dom_feat(:,1) = new_feat_data(:,1);
dom_feat(:,2) = new_feat_data(:,20);
dom_feat(:,3) = new_feat_data(:,22);
dom_feat(:,4) = new_feat_data(:,28);
dom_feat(:,5) = new_feat_data(:,29);
dom_feat(:,6) = new_feat_data(:,30);

[idx_test, C_test]=kmeans(dom_feat, 10);


dist = sqrt(sum((idx_test(1,:)-idx_tr(5,:)).^2))
for i=1:1:10
    temp = sqrt(sum((idx_test(i,:)-idx_tr(5,:)).^2))
    if(temp < dist )
        dist = temp;
        thesi =i;
    end
end
clearvars aa bb cc dd ee ff gg hh ii jj
aa = find(idx_test(:,1) == 1);
bb = find(idx_test(:,1) == 2);
cc = find(idx_test(:,1) == 3);
dd = find(idx_test(:,1) == 4);
ee = find(idx_test(:,1) == 5);
ff = find(idx_test(:,1) == 6);
gg = find(idx_test(:,1) == 7);
hh = find(idx_test(:,1) == 8);
ii = find(idx_test(:,1) == 9);
jj = find(idx_test(:,1) == 10);

sum0 = 0;
sum1 = 0;
sum2 = 0;
for i=1:1:size(aa,1)
    if(new_sessions{ aa(i) ,8} == 0)
        sum0 = sum0+1;
    end
    if(new_sessions{ aa(i) ,8} == 1)
         sum1 = sum1+1;
    end
    if(new_sessions{ aa(i) ,8} == 2)
         sum2 = sum2+1;
    end     
end

%clust 1 = label 2

sum0 = 0;
sum1 = 0;
sum2 = 0;
for i=1:1:size(bb,1)
    if(new_sessions{ bb(i) ,8} == 0)
        sum0 = sum0+1;
    end
    if(new_sessions{ bb(i) ,8} == 1)
         sum1 = sum1+1;
    end
    if(new_sessions{ bb(i) ,8} == 2)
         sum2 = sum2+1;
    end     
end
%clust 2 = label 2

sum0 = 0;
sum1 = 0;
sum2 = 0;
for i=1:1:size(cc,1)
    if(new_sessions{ cc(i) ,8} == 0)
        sum0 = sum0+1;
    end
    if(new_sessions{ cc(i) ,8} == 1)
         sum1 = sum1+1;
    end
    if(new_sessions{ cc(i) ,8} == 2)
         sum2 = sum2+1;
    end     
end
%clust 3 = label 2

sum0 = 0;
sum1 = 0;
sum2 = 0;
for i=1:1:size(dd,1)
    if(new_sessions{ dd(i) ,8} == 0)
        sum0 = sum0+1;
    end
    if(new_sessions{ dd(i) ,8} == 1)
         sum1 = sum1+1;
    end
    if(new_sessions{ dd(i) ,8} == 2)
         sum2 = sum2+1;
    end     
end
%clust 4 = label 2

sum0 = 0;
sum1 = 0;
sum2 = 0;
for i=1:1:size(ee,1)
    if(new_sessions{ ee(i) ,8} == 0)
        sum0 = sum0+1;
    end
    if(new_sessions{ ee(i) ,8} == 1)
         sum1 = sum1+1;
    end
    if(new_sessions{ ee(i) ,8} == 2)
         sum2 = sum2+1;
    end     
end
%clust 5 = label 1

sum0 = 0;
sum1 = 0;
sum2 = 0;
for i=1:1:size(ff,1)
    if(new_sessions{ ff(i) ,8} == 0)
        sum0 = sum0+1;
    end
    if(new_sessions{ ff(i) ,8} == 1)
         sum1 = sum1+1;
    end
    if(new_sessions{ ff(i) ,8} == 2)
         sum2 = sum2+1;
    end     
end
%clust 6 = label 2

sum0 = 0;
sum1 = 0;
sum2 = 0;
for i=1:1:size(gg,1)
    if(new_sessions{ gg(i) ,8} == 0)
        sum0 = sum0+1;
    end
    if(new_sessions{ gg(i) ,8} == 1)
         sum1 = sum1+1;
    end
    if(new_sessions{ gg(i) ,8} == 2)
         sum2 = sum2+1;
    end     
end
%clust 7 = label 2

sum0 = 0;
sum1 = 0;
sum2 = 0;
for i=1:1:size(hh,1)
    if(new_sessions{ hh(i) ,8} == 0)
        sum0 = sum0+1;
    end
    if(new_sessions{ hh(i) ,8} == 1)
         sum1 = sum1+1;
    end
    if(new_sessions{ hh(i) ,8} == 2)
         sum2 = sum2+1;
    end     
end
%clust 8 = label 2

sum0 = 0;
sum1 = 0;
sum2 = 0;
for i=1:1:size(ii,1)
    if(new_sessions{ ii(i) ,8} == 0)
        sum0 = sum0+1;
    end
    if(new_sessions{ ii(i) ,8} == 1)
         sum1 = sum1+1;
    end
    if(new_sessions{ ii(i) ,8} == 2)
         sum2 = sum2+1;
    end     
end
%clust 9 = label 2


sum0 = 0;
sum1 = 0;
sum2 = 0;
for i=1:1:size(jj,1)
    if(new_sessions{ jj(i) ,8} == 0)
        sum0 = sum0+1;
    end
    if(new_sessions{ jj(i) ,8} == 1)
         sum1 = sum1+1;
    end
    if(new_sessions{ jj(i) ,8} == 2)
         sum2 = sum2+1;
    end     
end
%clust 10 = label 2

 %%

correct_classifcations(8) = 9;
incorrect_classifcations(8) = 1;

correct_classifications_binary(8) = 10;
incorrect_classifications_binary(8) = 0;






