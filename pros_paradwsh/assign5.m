%% Data Filtering
%

load('first_dataset_clear.mat');
load('first_dataset_clear_features.mat');


N = size(new_sessions, 1);

%1. Video Watching Percentage Filter
vwp = feat_data(:, 25);

% Uncomment *ONE* of the lines below to 
% include this filter in your analysis. 
% #######################################################
discard_ids1 = find(vwp > 100 | vwp < 50);  %100 >= Vid Watch Perc >= 50
%discard_ids1 = find(vwp > 100 | vwp < 20); %100 >= Vid Watch Perc >= 20
%discard_ids1 = find(vwp > 100);            %100 >= Vid Watch Perc
% #######################################################


%1.1 Video Watching Duration >= 120sec. 
discard_ids11 = [];

% Uncomment the code below and rerun the script to 
% include this filter in your analysis. 
% #######################################################
% vwp = feat_data(:, 25);%Video Watching Percentage
% vd = feat_data(:, 24); %Video Duration
% vwd = vd.*vwp;  %Video Watching Duration
% discard_ids11 = find(not(vwd >= 120));
% #######################################################


%2. Missing/NaN values
discard_ids2 = [];
rb = new_sessions(:, 4); 
tl = cell2mat(new_sessions(:, 6));
sd = cell2mat(new_sessions(:, 7));
vd = cell2mat(new_sessions(:, 9));
ad = new_sessions(:, 14);
for i = 1:N
    
    if( numel(rb{i}) > 0)
        if( isnan(str2double(rb{i}(1, 1))) | isnan(str2double(rb{i}(1, 2))) )
            discard_ids2 = [discard_ids2 i];
            continue;
        end
    end
    
    if( isnan(tl(i)) | isnan(sd(i)) | isnan(vd(i)) )
        discard_ids2 = [discard_ids2 i];
            continue;     
    end
    
    if( numel(ad{i}) > 0)
        if( isnan(str2double(ad{i}(1, 1))) | isnan(str2double(ad{i}(1, 2))) )
            discard_ids2 = [discard_ids2 i];
            continue;
        end
    end
    
end


%3   Any sessions with Startup Delay > 0 must have: 
%    a rebuffering event at Timestamp = 0 with Duration > 0
rb = new_sessions(:, 4); 
sd = cell2mat(new_sessions(:, 7)); %Startup Delay
discard_ids3 = [];
for i = 1:N
    if( sd(i) == 0 ) %Is there Startup Delay > 0?
        continue;
    end
    if( numel(rb{i}) > 0)       %If there is a rebuffering event
        tmp_t = str2double(rb{i}(1, 1)) * 1000;    %Timestamp of event (ms)
        tmp_d = str2double(rb{i}(1, 2)) * 1000;    %Duration of event (ms)
        if not(tmp_t == 0 ) %If rb event isn't at timestamp 0, discard
            discard_ids3 = [discard_ids3 i]; 
            continue;
        end
        if( not( tmp_d > 0 ) )  %if dur = 0, discard
            discard_ids3 = [discard_ids3 i];    
            continue;
        end
    else %If there isn't a RB event, discard (since startup delay > 0)
        discard_ids3 = [discard_ids3 i];    
        continue;
    end

end


discard_ids_final = union(discard_ids1, discard_ids2);
discard_ids_final = union(discard_ids_final, discard_ids11);
discard_ids_final = union(discard_ids_final, discard_ids3);
new_sessions(discard_ids_final, :) = [];
feat_data(discard_ids_final, :) = [];


%% Production of extra features
%

%1. Total RBs Duration / Video Duration
%2. Starting Time of first RB event
%3. Starting Time of first RB event / Video Duration
%4. Mean RB Duration / Video Duration
%5. Median RB Duration / Video Duration
%6. Number of BR changes / Video Duration
%7. Timestamp of first BR change
%8. Timestamp of first BR change / Video Duration
%9. Startup Delay / Video Duration
%10. Ads Duration / Video Duration
%11. Number of skips / Video Duration


N = size(new_sessions, 1);

%1. Total RBs Duration / Video Duration
rbdur = feat_data(:, 2);
vdur = feat_data(:, 24);
f1 = rbdur ./ vdur;


%2. Starting Time of first RB event
rb = new_sessions(:, 4);
f2 = zeros(N, 1);
for i = 1:N
    if( numel(rb{i}) == 0 ) %If there are no RB events
        f2(i) = vdur(i);
        continue;
    end
    f2(i) = str2double(rb{i}(1, 1));
end


%3. Starting Time of first RB event / Video Duration
f3 = f2 ./ vdur;


%4. Mean RB Duration / Video Duration
mdur = feat_data(:, 7);
f4 = mdur ./ vdur;


%5. Median RB Duration / Video Duration
mdur = feat_data(:, 8);
f5 = mdur ./ vdur;


%6. Number of BR changes / Video Duration
nchanges = feat_data(:, 15);
f6 = nchanges ./ vdur;


%7. Timestamp of first BR change
br = new_sessions(:, 5);
f7 = zeros(N, 1);
for i = 1:N
    if( numel(br{i}) == 0 ) %If there are no RB events
        f7(i) = vdur(i);
        continue;
    end
    f7(i) = str2double(br{i}(1, 1));
end


%8. Timestamp of first BR change / Video Duration
f8 = f7 ./ vdur;


%9. Startup Delay / Video Duration
sd = cell2mat(new_sessions(:, 7));
f9 = sd ./ vdur;


%10. Ads Duration / Video Duration
addur = new_sessions(:, 14);
f10 = zeros(N, 1);
for i = 1:N
    if( numel(addur{i}) == 0 ) %If there are no RB events
        f10(i) = 0;
        continue;
    end
    f10(i) = str2double(addur{i}(1, 2)) / vdur(i);
end


%11. Number of skips / Video Duration
skips = cell2mat(new_sessions(:, 10));
f11 = skips ./ vdur;



%% Add new features/Remove old features 
%

N = size(new_sessions, 1);

nf_a = [1 2 4 7 8 9 10 12 14 15 16 17 18 19 20 21 22 23 24 27 28 29 30 31];
nf_b = [3 5 6 11 13 25 26 33 35 37];


new_features = [f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11];

new_feat_data = zeros(N, 37);
new_feat_data(:, nf_a) = feat_data(:, 1:numel(nf_a));
new_feat_data(:, nf_b) = feat_data(:, 1:numel(nf_b));
new_feat_data(:, 32) = cell2mat(new_sessions(:, 7));
new_feat_data(:, 36) = cell2mat(new_sessions(:, 10));

ad = new_sessions(:, 14);
for i = 1:N
    
    if( numel(ad{i}) == 0 ) %If there are no Ads
        new_feat_data(i, 34) = 0;
        continue;
    end
    
    new_feat_data(i, 34) = str2double(ad{i}(1, 2));

end


%% Lasso Regression - Feature Selection
%


X = new_feat_data;              %Features
Y = feat_data(:, 25);           %Target variable 25 = startupdel/viddur

i=1;
lambda = 0.0146;
[B, I] = lasso(X,Y,'Lambda', lambda); %lambda
b = sprintf(' %.10f ', abs(B(:,i)));
fprintf('Lambda=%.4f, Betas = [%s], MSE=%.4f\n',I.Lambda(i), b, I.MSE(i));

% Weights are stored in array 'B'
% Dominant features are features that have weight >= 1

for i=1:1:37
    if(abs(B(i))>0.1)
        i
    end
end   
%% kmeans 
dom_feat(:,1) = new_feat_data(:,1);
dom_feat(:,2) = new_feat_data(:,20);
dom_feat(:,3) = new_feat_data(:,22);
dom_feat(:,1) = new_feat_data(:,28);
dom_feat(:,2) = new_feat_data(:,29);
dom_feat(:,6) = new_feat_data(:,30);

for k=1:1:10

  idx(:,k)=kmeans(dom_feat, k);
 %eva = evalclusters(dom_feat,idx(:,k), silhouette);

end

 eva = evalclusters(dom_feat,'kmeans','silhouette','KList',[1:10]);

%% 10 fold

for i=1:1:4112
    part1{i,1} = new_sessions{i,1};
    part1{i,2} = new_sessions{i,2};
    part1{i,3} = new_sessions{i,3};
    part1{i,4} = new_sessions{i,4};
    part1{i,5} = new_sessions{i,5};
    part1{i,6} = new_sessions{i,6};
    part1{i,7} = new_sessions{i,7};
    part1{i,8} = new_sessions{i,8};
    part1{i,9} = new_sessions{i,9};
    part1{i,10} = new_sessions{i,10};
    part1{i,11} = new_sessions{i,11};
    part1{i,12} = new_sessions{i,12};
    part1{i,13} = new_sessions{i,13};
    part1{i,14} = new_sessions{i,14};
    part1{i,15} = new_sessions{i,15};
end
feat_data_part1 = feat_data(1:4112, : );


for i=4113:1:8224
    part2{i-4112,1} = new_sessions{i,1};
    part2{i-4112,2} = new_sessions{i,2};
    part2{i-4112,3} = new_sessions{i,3};
    part2{i-4112,4} = new_sessions{i,4};
    part2{i-4112,5} = new_sessions{i,5};
    part2{i-4112,6} = new_sessions{i,6};
    part2{i-4112,7} = new_sessions{i,7};
    part2{i-4112,8} = new_sessions{i,8};
    part2{i-4112,9} = new_sessions{i,9};
    part2{i-4112,10} = new_sessions{i,10};
    part2{i-4112,11} = new_sessions{i,11};
    part2{i-4112,12} = new_sessions{i,12};
    part2{i-4112,13} = new_sessions{i,13};
    part2{i-4112,14} = new_sessions{i,14};
    part2{i-4112,15} = new_sessions{i,15};
end
feat_data_part2 = feat_data(4113:8224, : );

for i=8225:1:12336
    part3{i-8224,1} = new_sessions{i,1};
    part3{i-8224,2} = new_sessions{i,2};
    part3{i-8224,3} = new_sessions{i,3};
    part3{i-8224,4} = new_sessions{i,4};
    part3{i-8224,5} = new_sessions{i,5};
    part3{i-8224,6} = new_sessions{i,6};
    part3{i-8224,7} = new_sessions{i,7};
    part3{i-8224,8} = new_sessions{i,8};
    part3{i-8224,9} = new_sessions{i,9};
    part3{i-8224,10} = new_sessions{i,10};
    part3{i-8224,11} = new_sessions{i,11};
    part3{i-8224,12} = new_sessions{i,12};
    part3{i-8224,13} = new_sessions{i,13};
    part3{i-8224,14} = new_sessions{i,14};
    part3{i-8224,15} = new_sessions{i,15};
end
feat_data_part3 = feat_data(8225:12336, : );

for i=12337:1:16448
    part4{i-12336,1} = new_sessions{i,1};
    part4{i-12336,2} = new_sessions{i,2};
    part4{i-12336,3} = new_sessions{i,3};
    part4{i-12336,4} = new_sessions{i,4};
    part4{i-12336,5} = new_sessions{i,5};
    part4{i-12336,6} = new_sessions{i,6};
    part4{i-12336,7} = new_sessions{i,7};
    part4{i-12336,8} = new_sessions{i,8};
    part4{i-12336,9} = new_sessions{i,9};
    part4{i-12336,10} = new_sessions{i,10};
    part4{i-12336,11} = new_sessions{i,11};
    part4{i-12336,12} = new_sessions{i,12};
    part4{i-12336,13} = new_sessions{i,13};
    part4{i-12336,14} = new_sessions{i,14};
    part4{i-12336,15} = new_sessions{i,15};
end
feat_data_part4 = feat_data(12337:16448, : );

for i=16449:1:20560
    part5{i-16448,1} = new_sessions{i,1};
    part5{i-16448,2} = new_sessions{i,2};
    part5{i-16448,3} = new_sessions{i,3};
    part5{i-16448,4} = new_sessions{i,4};
    part5{i-16448,5} = new_sessions{i,5};
    part5{i-16448,6} = new_sessions{i,6};
    part5{i-16448,7} = new_sessions{i,7};
    part5{i-16448,8} = new_sessions{i,8};
    part5{i-16448,9} = new_sessions{i,9};
    part5{i-16448,10} = new_sessions{i,10};
    part5{i-16448,11} = new_sessions{i,11};
    part5{i-16448,12} = new_sessions{i,12};
    part5{i-16448,13} = new_sessions{i,13};
    part5{i-16448,14} = new_sessions{i,14};
    part5{i-16448,15} = new_sessions{i,15};
end
feat_data_part5 = feat_data(16449:20560, : );

for i=20561:1:24672
    part6{i-20560,1} = new_sessions{i,1};
    part6{i-20560,2} = new_sessions{i,2};
    part6{i-20560,3} = new_sessions{i,3};
    part6{i-20560,4} = new_sessions{i,4};
    part6{i-20560,5} = new_sessions{i,5};
    part6{i-20560,6} = new_sessions{i,6};
    part6{i-20560,7} = new_sessions{i,7};
    part6{i-20560,8} = new_sessions{i,8};
    part6{i-20560,9} = new_sessions{i,9};
    part6{i-20560,10} = new_sessions{i,10};
    part6{i-20560,11} = new_sessions{i,11};
    part6{i-20560,12} = new_sessions{i,12};
    part6{i-20560,13} = new_sessions{i,13};
    part6{i-20560,14} = new_sessions{i,14};
    part6{i-20560,15} = new_sessions{i,15};
end
feat_data_part6 = feat_data(20561:24672, : );

for i=24673:1:28784
    part7{i-24672,1} = new_sessions{i,1};
    part7{i-24672,2} = new_sessions{i,2};
    part7{i-24672,3} = new_sessions{i,3};
    part7{i-24672,4} = new_sessions{i,4};
    part7{i-24672,5} = new_sessions{i,5};
    part7{i-24672,6} = new_sessions{i,6};
    part7{i-24672,7} = new_sessions{i,7};
    part7{i-24672,8} = new_sessions{i,8};
    part7{i-24672,9} = new_sessions{i,9};
    part7{i-24672,10} = new_sessions{i,10};
    part7{i-24672,11} = new_sessions{i,11};
    part7{i-24672,12} = new_sessions{i,12};
    part7{i-24672,13} = new_sessions{i,13};
    part7{i-24672,14} = new_sessions{i,14};
    part7{i-24672,15} = new_sessions{i,15};
end
feat_data_part7 = feat_data(24673:28784, : );

for i=28785:1:32896
    part8{i-28784,1} = new_sessions{i,1};
    part8{i-28784,2} = new_sessions{i,2};
    part8{i-28784,3} = new_sessions{i,3};
    part8{i-28784,4} = new_sessions{i,4};
    part8{i-28784,5} = new_sessions{i,5};
    part8{i-28784,6} = new_sessions{i,6};
    part8{i-28784,7} = new_sessions{i,7};
    part8{i-28784,8} = new_sessions{i,8};
    part8{i-28784,9} = new_sessions{i,9};
    part8{i-28784,10} = new_sessions{i,10};
    part8{i-28784,11} = new_sessions{i,11};
    part8{i-28784,12} = new_sessions{i,12};
    part8{i-28784,13} = new_sessions{i,13};
    part8{i-28784,14} = new_sessions{i,14};
    part8{i-28784,15} = new_sessions{i,15};
end
feat_data_part8 = feat_data(28785:32896, : );

for i=32897:1:37008
    part9{i-32896,1} = new_sessions{i,1};
    part9{i-32896,2} = new_sessions{i,2};
    part9{i-32896,3} = new_sessions{i,3};
    part9{i-32896,4} = new_sessions{i,4};
    part9{i-32896,5} = new_sessions{i,5};
    part9{i-32896,6} = new_sessions{i,6};
    part9{i-32896,7} = new_sessions{i,7};
    part9{i-32896,8} = new_sessions{i,8};
    part9{i-32896,9} = new_sessions{i,9};
    part9{i-32896,10} = new_sessions{i,10};
    part9{i-32896,11} = new_sessions{i,11};
    part9{i-32896,12} = new_sessions{i,12};
    part9{i-32896,13} = new_sessions{i,13};
    part9{i-32896,14} = new_sessions{i,14};
    part9{i-32896,15} = new_sessions{i,15};
end
feat_data_part9 = feat_data(32897:37008, : );

for i=37009:1:41120
    part10{i-37008,1} = new_sessions{i,1};
    part10{i-37008,2} = new_sessions{i,2};
    part10{i-37008,3} = new_sessions{i,3};
    part10{i-37008,4} = new_sessions{i,4};
    part10{i-37008,5} = new_sessions{i,5};
    part10{i-37008,6} = new_sessions{i,6};
    part10{i-37008,7} = new_sessions{i,7};
    part10{i-37008,8} = new_sessions{i,8};
    part10{i-37008,9} = new_sessions{i,9};
    part10{i-37008,10} = new_sessions{i,10};
    part10{i-37008,11} = new_sessions{i,11};
    part10{i-37008,12} = new_sessions{i,12};
    part10{i-37008,13} = new_sessions{i,13};
    part10{i-37008,14} = new_sessions{i,14};
    part10{i-37008,15} = new_sessions{i,15};
end
feat_data_part10 = feat_data(37009:41120, : );


%% here we use the files first/sec/third/for/fifth/six ...

%% mean median etc
correct_classifcations_mean = mean(correct_classifcations)
correct_classifcations_median = median(correct_classifcations)

incorrect_classifcations_mean = mean(incorrect_classifcations)
incorrect_classifcations_median = median(incorrect_classifcations)

correct_classifications_binary_mean = mean(correct_classifications_binary)
correct_classifications_binary_median = median(correct_classifications_binary)

incorrect_classifications_binary_mean = mean(incorrect_classifications_binary)
incorrect_classifications_binary_median = median(incorrect_classifications_binary)




